==============================================================================
TradeDangerous, Copyright (C) Oliver "kfsone" Smith, July 2014
==============================================================================

v6.2.2 [wip]
. (kfsone) Added "--max-days" to run command
+ Stations, ships: kfsone

v6.2.1 Dec 12 2014
. (kfsone) Added "olddata" command
. (kfsone) "run" (with -vv) will now show data age
. (kfsone) Gamma 2.0.5 renamed "Slaves" category to "Slavery"
. (kfsone) "sell" now has a --ages option too
. (kfsone) "buy" and "sell" --near now checks the station/system too
. (kfsone) "buy" now shows average cost if you specify --detail (-v)
. (kfsone) "sell" now shows average value if you specify --detail (-v)
. (kfsone) Fixed item name matching (--avoid)
. (kfsone) Fixed use of via in "run"
. (kfsone) Exposed cache.regeneratePricesFile()
. (kfsone) Call regeneratePricesFile() after calling plugin.finish()
. (kfsone) General code cleanup (removed buildLinks and loadTrades)
. (kfsone) Added VisualStudio pyproj (great for performance analysis)
+ Stations, distances, ship data: gazelle, gulasch, kfsone, mseven

v6.2.0 Dec 11 2014
. (kfsone) Added plugin system for "import", added a maddavo plugin.
. (kfsone) "-G" is now the default for "update"
. (kfsone) "--capacity" and "--ly-per" are now required "run" arguments
. (kfsone) "local --ly=0" now works as expected
. (kfsone) Tell the user when we rebulid the cache (-q to silence)
. (gulasch) Stations and distances
. (kfsone) Mineral and Microbial are now in the correct order,
. (kfsone) Big documentation cleanup,
. (mhuges) Added '--stations' to 'nav' to show a station count,
. (kfsone) Added '--ages' option to 'local' to show price data age,

v6.1.8 Dec 09 2014
. (kfsone) Huge performance improvement to "nav",
. (kfsone) Performance improvement to "run",
. (kfsone) #79 support for "--avoid" in the "nav" command,
. (kfsone) Download counter to "import", use -q to silence it,
. (kfsone) Fixed case-sensitivity matching station names that changed,
. (kfsone) 60 new stations,
. (kfsone) "update -G" now takes "-wx" and "-wy" arguments for setting the
  position of the GUI window. Try -wx=-220 -wy=220,
. (kfsone) Updated config.sh and tdupd to take advantage of -wx and -wy
. (eggplant) More stations and distances,

v6.1.7 Dec 07 2014
. (kfsone) Added "scripts" directory with bash scripts
  See scripts/README.txt
. (kfsone) Allow bigger price differences for most costly items in the UI,
  thanks to Bariri for the reports
. (eggplant) More station data

v6.1.6 Dec 06 2014
. (gulasch) Station additions, corrections and distances
. (kfsone) Added '--ages' option to "buy" command to show data age
. (kfsone) Fix for line-numbers in .prices files sometimes being incorrect
. (kfsone) Added "--start-jumps" (-s) option to "run" command which will
  use actively trading stations within that number of jumps of your
  specified system/station.
. (kfsone) Added "--empty" to "run" command to tell it how far you can
  jump when carrying zero cargo.
. (kfsone) Include origin system in "local" command output (works well
  with -v to list local stations)
. (kfsone) ui_order is now specified in Item.csv rather than per-item
  (Microbial Furnaces and Mineral Extractors should now be in UI order :)
. (gazelle) More Station data and some distances
. (eggplant) Made it possible to override the path to data files, etc,
  by specifying a dataDir= argument to TradeEnv(). Default is still ./data

v6.1.5 Dec 04 2014
. (kfsone) "run" --from will now accept a System name
. (kfsone) "update" GUI will now do some sanity checking on prices
. (kfsone) "nav" with -vv will show direct distance left to destination
. (kfsone) Minor speed improvements to "nav"
. (kfsone) Startup optimizations,
. (kfsone) "update" command now always exposes supply and demand,
. (kfsone) "--supply" (-S) is now deprecated for "update" command,

v6.1.4 Dec 01 2014
. (kfsone) "updated.prices" will now contain line removals
. (kfsone) 155 new systems from RedWizard
. (Eggplant!) More Stations
. (kfsone) Fix miscount of jumps when same-system hops were in a route,
. (kfsone) run --jumps=0 now limits to same-system options
. (kfsone) Fixed empty "Jump" lines in nav output for same-system hop

v6.1.3 Nov 30 2014
. (gazelle) More Station and Ship data
. (kfsone) Changed UPDATE command to use places, check for stations, and to
  generate a complete .prices fragment with timestamps for maddavo compat.
. (kfsone) Fixed "run" and "nav" not understanding stations correctly
. (kfsone) "avoid" now handles systems/stations discretely
  - avoiding a station means you won't land there but you can
    pass through the system,
  - avoiding a system means you won't land there or fly thru it,
. (kfsone) Improvements to name disambiguation:
  When specifying a "place" name (system or station) you can resolve
  any ambiguities with a new syntax:
    @name
      Means "this is a system name, don't conflict with stations"
    /name or @/name
      Means "this is a station, don't conflict with systems"
    name/name  or  @name/name
      Means "this is a system name and a station name"
. (kfsone) Improved matching algorithm

To see the difference, try:

  trade.py local --ly 5 bea
  trade.py local --ly 5 @bea
  trade.py local --ly 5 @/bea
  trade.py local --ly 5 prim/bea


v6.1.2 Nov 28 2014
. (kfsone) GUI now saves its updates as "updated.prices" so you can upload them
. (kfsone) "run" now accepts a system OR station as the destination
. (kfsone) System/Station name lookups are more friendly again
. (kfsone) '-q' now silences "unknown ..." warnings when importing with "-i"
. (gazelle) Improvements to .CSV file handling,
. (gazelle) Additional stations, data cleanup, etc.
. (kfsone) Fix for importing data without demand/supply
. (kfsone) Adding support for stations with the same name in diff systems
. (kfsone) Removed import-from-maddavo.py
. (kfsone) Added support for URLs in "import" sub-command
. (kfsone) Added "--maddavo" option to "import" to import maddavo's data
e.g.
  trade.py import -i http://kfs.org/td/prices
or
  trade.py import -i --maddavo


v6.1.1 Nov 27 2014
Optimization:
. (kfsone) Removed support for old pre v5 .prices files
. (kfsone) Streamlined demand/stock columns; these now only support:
  - and 0  for "not available",
  ?        for "who cares",
  nn[?LMH] for number of units and unknown, low, medium or high
(This makes the parser several times faster)
. (kfsone) Made parsing .prices file several times faster

v6.1.0 Nov 26 2014
. (gazelle) Added "export" sub-command to generate csv files of the current database
. (kfsone) Made the update-gui significantly more user friendly.
. (kfsone) Handle cases where .prices file is missing, e.g. bootstrap

v6.0.7 Nov 26 2014
. (kfsone) Removed 'pill' options from local command
. (kfsone) Friendly message instead of crash when local finds no stars
. (kfsone) Added "--ignore-unkown" (-i) to "import"
. (kfsone) "import" without a filename will now present an open file dialog
API:
. (kfsone) You can now have optional positional arguments in command lines

v6.0.6 Nov 25 2014
. (kfsone) Issue#63 Trade calculator was not seeing some non-trade jumps
. (maddavo/kfsone) Import of ~20,000 systems
. (kfsone) GUI tweaks
. (kfsone) Station cleanup
. (kfsone) Various new stations
. (kfsone) Various minor bug fixes

v6.0.5 Nov 22 2014 (w/Gamma 1)
. (kfsone) Added "import" sub-command for loading 1+ stations from a file
. (kfsone) Added "--height" (-H) option to update GUI
. (kfsone) Added "--front" (-F) to keep the update gui infront of TD
. (kfsone) Added Pilot's Federation/Jameson Memorial
. (kfsone) Added '--ignore-unknown' option to buildcache

v6.0.4 Nov 21 2014 (Beta 3.9.1)
. (kfsone) Added "sell" command (find places to sell a specific item)
. (kfsone) Reactive Armor changed to Reactive Armour
. (kfsone) Added 'Chemical Waste'
. (kfsone) Update GUI now consistently selects text on entering a cell
. (kfsone) Update GUI now selects the first column of the first line on startup
. (kfsone) Update GUI now supports up/down key inputs and has a default size
. (kfsone) Issue#57 Fixed extra cache rebuilds after doing an 'update'
. (kfsone) Changing .prices won't force a full rebuild of the cache
. (kfsone) Better integrated update gui (improved startup times)
. (kfsone) Added "--all" (-A) option to 'update -G' to show ALL items
. (kfsone) (Update GUI) Set 'selling' and 'asking' to 0 to delete an item
. (kfsone) Fixed "buy" command showing multiple hits for multi-station systems

v6.0.3 Nov 21 2014
. (kfsone) Added a GUI to the "update" sub-command, it's experimental:
   - Use <tab>/<shift-tab> to cycle across rows,
   - Use <enter> to drop to the first column of the next line,
   - Scrolling is currently broken - you have to manually scroll. Ick.
   - Use the "--exp" flag to indicate your understanding it is experimental.
     e.g.: trade.py update --exp aulin
. (kfsone) Fixed sqrt crash in buildCache.py in multi-station systems,
. (kfsone) Fixed "local" command not showing stations when using "--detail",
. (kfsone) Renamed "buildcache.py" to "cache.py"

BETA 3: 
CAUTION: So many systems changed with the Beta 3 update that old prices files
won't stand a chance of containing many valid systems. You may want to obtain
a fresh file from one of the online sources, e.g.
maddavo's site: http://www.davek.com.au/td/default.asp

v6.0.2 Nov 20 2014
. (maddavo) Imported additional systems
. (kfsone) Big performance improvements
. (kfsone) Added '--limit' to 'buy' sub-command
. (kfsone) Stop forcing "Chu hub" to be "Chi hub" (Styx)
. (kfsone) Improved speed of cache build

v6.0.1 Nov 16 2014 (kfsone)
. Added "--end" option to "run" for multiple-choice route termination.
. Database refactor to improve runtime performance
-- Some commands were taking upto 50s to load data from the cache.
-- By reorganizing some data and adding better indexes, the run command,
-- for example, went from 60s+ with a full prices file down to 3-4.
. Assorted minor functionality and perf improvements,
. Various bug fixes

NOTE: Rebuilding the cache can now take 2-8 seconds, but you only
pay this price on the first usage after you import a new .prices file
or update a station.

Developers:
. TradeDB.buildLinks now reads from the database instead of building
the link table itself. Reduced execution time from 600+ms to 30ms.
. TradeCalc.getBestHops will lazily load trades for the stations it
is considering, so you do not need to call loadTrades, but you do
still need to clal buildLinks at this time.
. TradeDB now provides loadStationTrades([stationIDs]) which will
load profitable trades *from* the specified list of stationIDs.
. To test for a station not having loaded its trades:
    if stn.tradingWith is None:
        # Not loaded

v6.0.0 Nov 09 2014 (kfsone)
. Major overhaul of command line interface and the internal command API.

User Facing:
- Numerous significant performance improvements, esp loading times,
- Improved command-line feedback in assorted error conditions,
- Various changes to support incoming data changes such as renaming
 of items, categories, stations, etc,
- Improved robustness around a lot of rough-edge cases,
- "common" args (-v, -w) now come after the command (e.g. update -v),
- "-q" has more effect (try 'nav' with -q -q),
- "update" has better feedback on errors,
- "update" will save your update as "prices.last" if there is an error
  (weren't you tired of losing all your changes to a typo?)
- "nav" command overhauled,
- "local" command overhauled,

Developer:
- Sub-commands are now implemented in individual modules in the commands/
 folder and suffixed _cmd (e.g. commands/buy_cmd.py)
 See also commands/TEMPLATE.py for command boilerplate.
 See commands/__init__.py for the command list,
- Added "TradeEnv" container for transporting settings and arguments,
- Added "CommandEnv" for parsing command line and populating a TradeEnv
 based on the command-line origins (step towards user settings),
- sub-commands are now split into a "run" and "render" phase so that
 developers can leverage their functionality without having to parse
 textual output. See "commands/buy_cmd.py" for an example,
- Added "nose" based Unit Tests,
- Made "buildcache" a trade.py sub-command rather than a standalone
 tool. Use this if changes you make break your .db and for some reason
 trade.py won't boot-strap itself. ('trade.py buildcache')
TODO:
. Finish implementing tests,
. Make "update" have an option that just takes a filename to allow
 people to import data from elsewhere more easily,
. Document the command API in TEMPLATE.py

v5.0.1 Nov 4/2014
. (kfsone) Issue #49 Errors in Ambiguity Error
. (kfsone) Issue #51 L and ? items weren't honoring qty limits
. (kfsone) Issue #50 Interaction between -0 and demand for a sold item
. (Smacker65) Beta 3 Systems with markets

v5.0.0 Oct 31/2014
. (kfsone) Initial Beta 3 changes
 - Improved the "corrections" system which facilitates changing names
   between versions, you can now "correct" System, Station, Category
   and Item names;
 - Renamed "Drugs" to "Legal Drugs", guess we need to add "Illegal Drugs"
 - Renamed some obvious items (Hel-Static Furnaces -> Microbial Furnaces),
 - Added some items that looked new (but could be renames),
 - Renamed Hopkins Hangar -> Cori Terminal

v4.7.0 Oct 26/2014
. (kfsone) Added "buy" sub-command for looking up sales of an item

v4.6.3 Oct 26/2014
. (kfsone) Fixed distance-related breakage in 4.6.2
. (kfsone) Improved error feedback while processing .prices file

v4.6.2 Oct 25/2014
. (kfsone) Added support for self-correcting star/station name changes,
. (kfsone) Added name corrections for maddavo's current TradeDangerous.prices,
. (kfsone) Assorted minor API changes,
. (kfsone) Minor startup optimization pass

v4.6.1 Oct 25/2014
. (kfsone) Added "--supply" (-S) which shows supply (demand and stock) fields when editing,
. (kfsone) Added "--timestamps" (-T) which shows timestamp field when editing,
. (kfsone) Added "--force-na" (-0) which replaces 'unk' values with 'n/a' (use with care),
. (kfsone) Deprecated "--all" and "--zero",
. (kfsone) Minor fixes to new .prices format

v4.6.0 Oct 24/2014
. (kfsone) New extended .prices format:
  <item name> <sell> <buy> <demand> <stock> [<time>]
  Demand/stock can be:
   unk     :-  "unknown" - treat as though always available
   n/a     :-  "not available" - ignore in trade calcs
   0       :-  alias for "n/a"
  or the number of units followed by L, M or H, e.g.
   10L     :- 10 units at Low
   500M    :- 500 units at Medium
   9000H   :- 9000 units at High
  Note that the time has moved to the end of the line.
  When updating data, you can either remove the time and have it
  default to 'now' or you can explicitly write the word now.
. (smacker65) Couple of station name corrections
. (kfsone) Better feedback when price data could not be found
. (kfsone) .prices wiki page
. (smacker65) More star data and corrections
. (gazelle) Fix for "--zero" and, e.g., "-1L0"

v4.5.1 Oct 20/2014
. (kfsone) Added --dir (-C) command line for specifying which directory you
  want trade.py to look in for it's files.
. (kfsone) trade.py will now default to trying to look for files relative
  to where the "trade.py" script was called from. So if you run
  "c:\tradedangerous\trade.py" it will look for the data directory as
  "c:\tradedangerous\data\" (Issue #39)

v4.5.0 Oct 20/2014
. (Smacker65/Community) Smacker brings the star database up to 567 systems.

v4.4.0 Oct 19/2014
. (Gazelle/Community) Merged Gazelle's update with community sources Star/Station
  data. Thanks to Gazelle, Smacker, RedWizzard, Haringer, Wolverine.
  It's only data but it's a big update and a lot of work went into it :)

v4.3.0 Oct 17/2014
. (gazelle) Added "--zero" option to "update --all" which makes the default
   value for demand/stock levels "0" instead of "-1". (-1 means 'unknown'.
   Use this option if you are editing timestamps and stock levels and all
   those '-'s get in your way)
. (gazelle) Moved Star, System, etc data out of ".sql" file and into
   per-table ".csv" files. This should make it much easier for people to
   share and synchronize data and make it less daunting for people to
   maintain their data files. Great work, Gazelle!

v4.2.3 Oct 17/2014
. (ShadowGar, Smacker65) Imported Harbinger and RedWizzard system data,
  also added tracking of where data has come from. Thanks also to Wolverine
  for his work identifying data sources.

v4.2.2 Oct 16/2014
. (kfsone) user-friendlified errors generated by TD
. (kfsone) gave "update" sub-command a "--npp" option (notepad++)
. (kfsone) gave "update" sub-command a "--vim" option (vim editor)

v4.2.1 Oct 15/2014
. (ShadowGar) Added more Stars and corrected HAGALAZ
. (kfsone) sort items by name rather than id when building .prices files
. (kfsone) improved the instructional comments in .prices files
. (kfsone) Big README.txt cleanup

v4.2.0 Oct 15/2014
. (Smacker65) Added "local" sub-command (lists systems within jump range of you)

v4.1.0 Oct 15/2014
. (gazelle) ".prices" file is no-longer included in the git repository,
  instead you'll need to download one or populate your own
. (gazelle) added the "--all" option to the "update" sub-command,
. (kfsone) fixed problems caused when the .prices file is missing and there is
  limited or no data available - user should get better feedback now.

v4.0.4 Oct 15/2014
. (kfsone) Issue #20 Improved fuzzy-matching of system/star names
. (gazelle) Fixed "Lacaille Prospect"
. (kfsone) "trade.py" is now executable by default on Linux/MacOS

v4.0.3 Oct 12/2014
. (kfsone) Issue #17 "--avoid gold" conflicted with "Goldstein Mines"
. (Smacker65) Issue #13 "Nelson Port" was listed as "Nelson Point"
. (kfsone) Issue #12 "-w" was failing because Rigel has no links
. (kfsone) Issue #11 Partial name matches weren't generating an ambiguity (e.g. 'ra' or 'ross')
. (kfsone) Issue #19 Beryllium and Gallium were incorrectly identified as Minerals

v4.0.2 Oct 06/2014
. (ShadowGar) More systems/stations from

v4.0.1 Oct 06/2014
. (kfsone)  Improved "--sublime" option, now supports Sublime Text 2 and works under Mac/Lin

v4.0 Oct 05/2014
. (ShadowGar) Updated to Beta 2 - All credit to ShadowGar

v3.9 Sep 21/2014
  NOTE: '--detail' (-v) is now a common option, i.e. must come before sub-command
    trade.py cleanup -v    << won't work any more
    trade.py -v cleanup    << new order
  Added 'nav' sub-command for navigation assistance:
    trade.py nav [--ship name [--full]] [--ly-per] from to
  Added '--quiet' (-q) common option for reducing level of detail, e.g.
    trade.py -q cleanup

v3.8 Sep 17/2014
  Fix for Issue #7: --avoid not working with systems that have no stations,
  Additional help text,
  General cleanup,
  Running "emdn-tap.py -v" will show price changes,

v3.7 Sep 15/2014
  Fixed excessive CPU usage in emdn-tap.py (ty, jojje)
  Added 'cleanup' command to help remove bad data from emdn
    EMDN isn't foolproof, and sometimes receives invalid or
    deliberately poisoned data. The best way to detect this
    is currently to look for prices that are somewhat older
    than the rest of the information received for the same
    station. This command checks for those records and
    removes them.
  emdn-tap now tries harder to honor commit intervals.
API Changes:
  makeSubParser now takes an epilog  

v3.6 Sep 12/2014
  Added DB support for tracking item stock/demand levels,
  TradeCalc will now factor stock levels when present,
  Minor performance/memory tweak
  emdn-tap:
    Now accepts --warn-to argument,
    Applies filters to what data it will accept,
    Records item stock/demand levels to the DB

v3.5 Sep 06/2014
  The emdn-tap tool now uses the compressed JSON stream rather than
  the CSV stream - saves you bandwidth.

v3.4 Sep 05/2014
  Added emdn-tap.py script which pulls data from EMDN network.
  "--via" now accepts multiple and/or comma separated stations so you can plan
  a very specific shopping-list route.

v3.3 Sep 04/2014
  Updated README to include sub-commands,
  Fixed a 'file not found' error running trade.py the first time with no arguments,
  Made specific CATEGORY/Item lookups possible (e.g. "Metals/Gold"),
  Added games internal names for items to the database,
  Enabled internal-name lookups for items (e.g. 'heliostaticfurnaces'),
  Fixed a bug where two names for the same thing caused ambiguity (duh!),
API changes:
  TradeDB.listSearch() now also takes a val() argument,
  Added a simple EMDN access module (emdn directory),
  Cleaned up various __repr__ functions now I know what __repr__ is for,

v3.2 Sep 03/2014
  Internal cleanup of how we process sub-commands

v3.1 Sep 01/2014
  Introduced sub-commands:
  - "run" command provides the old default behavior of calculating a run,
  - "update" command provides ways to update price database easily,

v3.0 Aug 30/2014
  Major overhaul. No-longer requires Microsoft Access DB. To update prices,
  edit the data/TradeDangerous.prices file. When you next run the tool, new
  data will be loaded automatically.
  Cleaned up code, normalized the way I name functions, etc.
  Added more ship data, etc.
